def subList(listTest, listParent):
    for element in listTest:
        if element.lower() not in [item.lower() for item in listParent]:
            return False
    return True

def refineDataList(list):
    list.pop(0)
    list.pop(0)
    for i in range(len(list)):
        list[i]['Rating'] = float(list[i]['Rating'])
        list[i]['Votes'] = float(list[i]['Votes'])

        if (len(list[i]['Revenue']) == 0):
            list[i]['Revenue'] = 0
        else:
            list[i]['Revenue'] = float(list[i]['Revenue'])

        if (len(list[i]['Metascore']) == 0):
            list[i]['Metascore'] = 0
        else:
            list[i]['Metascore'] = float(list[i]['Metascore'])

        list[i]['Year'] = int(list[i]['Year'])
        list[i]['Runtime'] = int(list[i]['Runtime'])




def loadLocalDB():
    fh = open('movieused.desc.csv', 'r')
    movieListFromFile = []
    for line in fh:
        movieListFromFile.append(line.strip().split(','))
    movieList = []
    for oneMovie in movieListFromFile:
        movie_dict = {}
        movie_dict['Title'] = oneMovie[1]
        movie_dict['Genre'] = oneMovie[2].replace('&', ',', 999).split(',')
        movie_dict['Description'] = oneMovie[3]
        movie_dict['Rating'] = oneMovie[4]
        movie_dict['Votes'] = oneMovie[5]
        movie_dict['Revenue'] = oneMovie[6]
        movie_dict['Metascore'] = oneMovie[7]
        movie_dict['Director'] = oneMovie[8]
        movie_dict['Actors'] = oneMovie[9].replace('&', ',', 999).split(',')
        movie_dict['Year'] = oneMovie[10]
        movie_dict['Runtime'] = oneMovie[11]

        movieList.append(movie_dict)
    refineDataList(movieList)
    return movieList


class MovieDB(object):
    '''
        The database class for movie data
        note: All return list are shadow copys, no change to the movie data dict is allowed
    '''
    def __init__(self, movieList):
        self.movieList = movieList

    def getMovieList(self):
        return self.movieList.copy()

    def filterDirector(self, list, director, exclude = False):
        if (exclude):
            mathedList = [movie for movie in list if movie['Director'] != director]
        else:
            mathedList = [movie for movie in list if movie['Director'] == director ]
        return mathedList.copy()

    def filterTitle(self, list, movieTitle, exclude = False):
        if (exclude):
            nameMathList = [movie for movie in list if movie['Title'] != movieTitle]
        else:
            nameMathList = [movie for movie in list if movie['Title'] == movieTitle ]
        return nameMathList.copy()

    def filterGenreActor(self, list, key, values, exclude = False):
        #convert the string into list
        if isinstance(values, str):
            values = list(values)
        if key not in ['Genre', 'Actors']:
            raise ValueError('The key value can only be either Genre or Actors')
        if (exclude):
            mathedList = [movie for movie in list if (not(subList(values, movie[key])))]
        else:
            mathedList = [movie for movie in list if (subList(values, movie[key]))]
        return mathedList.copy()


    def filterRange(self, list, key, upperLimit = 99999999999999, lowerLimit = 0, exclude = False):
        # allowed key list
        allowedList = ['Rating', 'Votes', 'Revenue', 'Metascore', 'Year', 'Runtime']
        if key not in allowedList:
            raise ValueError('The key is not in the allowed list')
        if (exclude):
            mathedList = [movie for movie in list if (movie[key] >= upperLimit or movie[key] <= lowerLimit)]
        else:
            mathedList = [movie for movie in list if (movie[key] <= upperLimit and movie[key] >= lowerLimit)]
        return mathedList.copy()

    def sortList(self, list, sortKey, reverse = False):
        # sort list according to the given key
        if (len(list) == 0):
            return []
        if sortKey not in list[0]:
            return []
        return sorted(list, key=lambda x: x[sortKey], reverse=reverse)

    def tips1(self):
        # returned the list sorted as revenue
        sortedList = self.sortList(self.movieList, 'Revenue', reverse=True)
        first10 = sortedList[0:10]
        restList = sortedList[10:]
        first10Revenue = sum(item['Revenue'] for item in first10)
        restRevenue = sum(item['Revenue'] for item in restList)
        chartDataSet = {
            "type": 'pie',
            "keySet": ['First 10 moives', 'Rest movies'],
            "value": [first10Revenue, restRevenue],
            "description": 'None'
        }
        return chartDataSet



if __name__ == '__main__':
    movieList = loadLocalDB()
    movieDB = MovieDB(movieList)
    sortedList = movieDB.tips1()